import React from 'react';
import cn from 'classnames';
import { Link } from 'gatsby';
import css from './style.module.scss';

const Button = ({
  to,
  children,
  onClick,
  className,
  type,
  link = false,
  secondary,
  big,
  small,
  external,
  style,
  target,
}) => {
  const classNames = cn(
    link ? css.link : css.button,
    secondary && css.secondary,
    big && css.big,
    small && css.small,
    className,
  );
  if (to) {
    if (external) {
      return (
        <a target={target} className={classNames} style={style} href={to}>
          {children}
        </a>
      );
    }
    return (
      <Link className={classNames} style={style} to={to}>
        {children}
      </Link>
    );
  }
  return (
    <button type={type || 'button'} className={classNames} style={style} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
