import React, { useState } from 'react'
import css from './style.module.scss'
const BottomBar = ({ children }) => {
  return <div className={css.bottomBar}>{children}</div>
}

export default BottomBar
