import React from 'react'
import { UpTitle, Title } from 'components/ui/Typography'
import css from './style.module.scss'

export const Section = ({ upTitle, title, children }) => (
  <div className={css.section}>
    {upTitle && <UpTitle>{upTitle}</UpTitle>}
    <Title>{title}</Title>
    {children}
  </div>
)

export default Section
