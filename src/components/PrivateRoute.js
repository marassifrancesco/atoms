import React from 'react'
import { checkIsSignedIn } from 'utils/auth'
import { navigate } from 'gatsby'
import { UserSession } from 'blockstack'
import UserSignedInContainer from './UserSignedInContainer'

class PrivateRoute extends React.Component {
  state = { checking: true, signedIn: false }

  componentDidMount = () => {
    const { location } = this.props
    checkIsSignedIn().then(signedIn => {
      if (location.pathname !== `/app/login`) {
        if (!signedIn) {
          // If the user is not logged in, redirect to the homepage.
          navigate(`/`)
          return null
        } else {
          if (location.search && location.search.startsWith('?authResponse=')) {
            navigate(`/app`)
          }
        }
      }
      this.setState({ checking: false, signedIn })
    })
  }

  render() {
    const { component: Component, location, ...rest } = this.props
    const { checking, signedIn } = this.state
    if (checking) {
      return <>...</>
    } else {
      return signedIn ? (
        <UserSignedInContainer>
          <Component {...rest} />
        </UserSignedInContainer>
      ) : null
    }
  }
}

export default PrivateRoute
