var proxy = require('http-proxy-middleware')
const path = require('path')

module.exports = {
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`/app/*`] },
    },
    `gatsby-plugin-sass`,
    {
      resolve: 'gatsby-plugin-svgr-loader',
      options: {
        rule: {
          include: /icons/, // See below to configure properly
        },
      },
    },
    {
      resolve: 'gatsby-plugin-root-import',
      options: {
        src: path.join(__dirname, 'src'),
        api: path.join(__dirname, 'src/api'),
        components: path.join(__dirname, 'src/components'),
        // pages: path.join(__dirname, 'src/pages'),
        utils: path.join(__dirname, 'src/utils'),
        style: path.join(__dirname, 'src/style'),
        reducers: path.join(__dirname, 'src/reducers'),
      },
    },
  ],
}
